﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Gives us access to unity's built in UI tools
using UnityEngine.UI;
public class ShowUI : MonoBehaviour
{
    public Canvas overlayScreen;

    void OnTriggerEnter(Collider TheThing)
    {
        if (TheThing.tag == "Player")
        {
            //hide the UI canvas
            overlayScreen.enabled = true;
            Debug.Log("You entered the area");
        }
    }

    void OnTriggerExit(Collider TheThing)
    {
        if (TheThing.tag == "Player")
        {
            //hide the UI canvas
            overlayScreen.enabled = false;
            Debug.Log("You left the area");

        }
    }

}
